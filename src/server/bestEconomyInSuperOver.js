//[{bowler: , total_runs: , ball: }];
//[{bowler: , economy_rate: }]

function bestEconomyInSuperOver(matches,deliveries){
    let bestEconomy=deliveries.reduce(function(bowlerArray,delivery){
        if(delivery['is_super_over']===1){
            let bowlerIndex=bowlerArray.findIndex(function(bowler){
                return bowler['bowler']===delivery['bowler'];
            });
            if(bowlerIndex!==-1) {
                bowlerArray[bowlerIndex]['total_runs']+=delivery['total_runs'];
                bowlerArray[bowlerIndex]['ball']+=1;
            }else{
                let bowler={};
                bowler['bowler']=delivery['bowler'];
                bowler['total_runs']=delivery['total_runs'];
                bowler['ball']=1;
                bowlerArray.push(bowler);
            }

        }
        return bowlerArray;
    },[]).map(function(bowler){
        let bowlerObject={};
        bowlerObject['bowler']=bowler['bowler'];
        bowlerObject['economy_rate']=Math.ceil(bowler['total_runs']/(bowler['ball']/6));
        return bowlerObject;
    }).sort(function(a,b){
        return a['economy_rate']-b['economy_rate'];
    }).slice(0,1);
   
    
    return  bestEconomy;
   
}


module.exports=bestEconomyInSuperOver;