//matchIdList=[{season:2016  , id:[577,578]}];
//strikeList={batsman: , strike:[{season: ,total_runs: ,ball: }]};
//playerStrikeList={batsman: , strike:[{season: ,strike_rate: }]}


function strikeRateOfBatsman(matches,deliveries){
    
    let matchIdList=matches.reduce(function(resultArray,match){
        let index = resultArray.findIndex(function (value) {
               return value['season'] === match['season'];
            });
            if (index != -1) {
                 resultArray[index]['id'].push(match['id']);
                } else {
                let seasonObject = {};
                seasonObject['season'] = match['season'];
                seasonObject['id'] = [];
                seasonObject['id'].push(match['id']);
               resultArray.push(seasonObject)
            }
            return resultArray;
        },[]);
        // console.log(matchIdList);
        
        let batsman="DA Warner";
        let strikeList=deliveries.reduce(function(resultArray,delivery){
        let m=matchIdList.find(function(matchId){
           if(matchId['id'].includes(delivery['match_id'])){
            return matchId;
           }
         });
        
        let season=m['season'];
        if(batsman===delivery['batsman']){
            if(resultArray['batsman']){
                let strike= resultArray['strike'];
                let seasonIndex = strike.findIndex(function (seasonObj) {
                    return seasonObj['season'] === season;
                });
              
                if (seasonIndex !== -1) {
                    strike[seasonIndex]['total_runs'] += delivery['total_runs'];
                    strike[seasonIndex]['ball'] += 1;
                } else {
                     let seasonObject = {};
                     seasonObject['season'] = season;
                     seasonObject['total_runs'] =delivery['total_runs'];
                     seasonObject['ball']=1;
                     strike.push(seasonObject);
                    }
                } else{
                    resultArray['batsman']=batsman;
                    resultArray['strike']=[];
                    let seasonObject={};
                    seasonObject['season']=season;
                    seasonObject['total_runs']=delivery['total_runs'];
                    seasonObject['ball']=1;
                    resultArray['strike'].push(seasonObject);
                }
           }
        return resultArray;
     },{});
    //  console.log(strikeList);
     let strike=strikeList['strike'].map(function(strikeObj){
         let seasonObject={};
         seasonObject['strike_rate']= ((strikeObj['total_runs']/strikeObj['ball'])*100).toFixed(2);
         seasonObject['season']=strikeObj['season'];
            return seasonObject;
        });
        let playerStrikeList={};
        playerStrikeList['batsman']=batsman;
        playerStrikeList['strike']=strike;
        return playerStrikeList;
    }


module.exports=strikeRateOfBatsman;