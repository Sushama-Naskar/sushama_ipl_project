let fs=require('fs');

let matches=require('../../data/matchesJson.json');
let matchesPlayedPerYear=require('../matchesPlayedPerYear.js');

let matchesCountList=matchesPlayedPerYear(matches);
console.log(matchesCountList);

//check
let actualResult=JSON.stringify(matchesCountList);
let expectedResult={
    '2008': 58,
    '2009': 57,
    '2010': 60,
    '2011': 73,
    '2012': 74,
    '2013': 76,
    '2014': 60,
    '2015': 59,
    '2016': 60,
    '2017': 59
  };
  
  if(actualResult ===JSON.stringify(expectedResult)){
      fs.writeFile('../../public/output/matchesPlayedPerYear.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
              console.log('Data is stored at matchesPlayedPerYear.json file');
          }
          
      });


  }else{
      console.log('Wrong output');
  }
