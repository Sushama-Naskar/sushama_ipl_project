let fs = require('fs');

let matches = require('../../data/matchesJson.json');
let teamWonTossAndMatch = require('../teamWonTossAndMatch.js');

let winnerCountList = teamWonTossAndMatch(matches);
console.log(winnerCountList);

//check
let actualResult = JSON.stringify(winnerCountList);
let expectedResult = {
    'Rising Pune Supergiant': 5,
    'Kolkata Knight Riders': 44,
    'Kings XI Punjab': 28,
    'Royal Challengers Bangalore': 35,
    'Sunrisers Hyderabad': 17,
    'Mumbai Indians': 48,
    'Gujarat Lions': 10,
    'Delhi Daredevils': 33,
    'Chennai Super Kings': 42,
    'Rajasthan Royals': 34,
    'Deccan Chargers': 19,
    'Kochi Tuskers Kerala': 4,
    'Pune Warriors': 3,
    'Rising Pune Supergiants': 3
};

if (actualResult === JSON.stringify(expectedResult)) {
      fs.writeFile('../../public/output/teamWonTossAndMatch.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
    console.log('Data is stored at teamWonTossAndMatch.json file');
          }

      });


} else {
    console.log('Wrong output');
}
