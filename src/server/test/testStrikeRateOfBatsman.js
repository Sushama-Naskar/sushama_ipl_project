let fs=require('fs');

let matches=require('../../data/matchesJson.json');
let deliveries=require('../../data/deliveriesJson.json');
let strikeRateOfBatsman=require('../strikeRateOfBatsman.js');

let strikeRate=strikeRateOfBatsman(matches,deliveries);

console.log(strikeRate);
// //check
let actualResult=JSON.stringify( strikeRate);
let expectedResult={
      batsman: 'DA Warner',
      strike: [
        { strike_rate: '145.02', season: 2017 },
        { strike_rate: '124.82', season: 2009 },
        { strike_rate: '148.21', season: 2010 },
        { strike_rate: '122.42', season: 2011 },
        { strike_rate: '175.16', season: 2012 },
        { strike_rate: '128.53', season: 2013 },
        { strike_rate: '144.04', season: 2014 },
        { strike_rate: '160.05', season: 2015 },
        { strike_rate: '152.68', season: 2016 }
      ]
    };

  if(actualResult ===JSON.stringify(expectedResult)){
      fs.writeFile('../../public/output/strikeRateOfBatsman.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
              console.log('Data is stored at strikeRateOfBatsman.json file');
          }
          
      });


  }else{
      console.log('Wrong output');
  }