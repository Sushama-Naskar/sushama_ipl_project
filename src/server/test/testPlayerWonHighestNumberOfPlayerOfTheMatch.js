let fs = require('fs');

let matches = require('../../data/matchesJson.json');
let PlayerWonHighestNumberOfPlayerOfTheMatch = require('../playerWonHighestNumberOfPlayerOfTheMatch.js');

let winnerCountList = PlayerWonHighestNumberOfPlayerOfTheMatch(matches);
// console.log(winnerCountList);

//check
let actualResult = JSON.stringify(winnerCountList);

let expectedResult = [
  { season: 2017, player: 'BA Stokes' },
  { season: 2008, player: 'SE Marsh' },
  { season: 2009, player: 'YK Pathan' },
  { season: 2010, player: 'SR Tendulkar' },
  { season: 2011, player: 'CH Gayle' },
  { season: 2012, player: 'CH Gayle' },
  { season: 2013, player: 'MEK Hussey' },
  { season: 2014, player: 'GJ Maxwell' },
  { season: 2015, player: 'DA Warner' },
  { season: 2016, player: 'V Kohli' }
];

if (actualResult === JSON.stringify(expectedResult)) {
      fs.writeFile('../../public/output/PlayerWonHighestNumberOfPlayerOfTheMatch.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
    console.log('Data is stored at testPlayerWonHighestNumberOfPlayerOfTheMatch.json file');
          }

      });


} else {
    console.log('Wrong output');
}
