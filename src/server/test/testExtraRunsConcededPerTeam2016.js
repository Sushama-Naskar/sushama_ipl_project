let fs=require('fs');

let matches=require('../../data/matchesJson.json');
let deliveries=require('../../data/deliveriesJson.json');
let extraRunsConcededPerTeam2016=require('../extraRunsConcededPerTeam2016.js');

let extraRunList=extraRunsConcededPerTeam2016(matches,deliveries);
console.log(extraRunList);

//check
let actualResult=JSON.stringify(extraRunList);
let expectedResult={
  'Rising Pune Supergiants': 108,
  'Mumbai Indians': 102,
  'Kolkata Knight Riders': 122,
  'Delhi Daredevils': 106,
  'Gujarat Lions': 98,
  'Kings XI Punjab': 100,
  'Sunrisers Hyderabad': 107,
  'Royal Challengers Bangalore': 156
};

  if(actualResult ===JSON.stringify(expectedResult)){
      fs.writeFile('../../public/output/extraRunsConcededPerTeam2016.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
              console.log('Data is stored at extraRunsConcededPerTeam2016.json file');
          }
          
      });


  }else{
      console.log('Wrong output');
  }
