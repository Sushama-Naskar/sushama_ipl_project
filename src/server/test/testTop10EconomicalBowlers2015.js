let fs=require('fs');

let matches=require('../../data/matchesJson.json');
let deliveries=require('../../data/deliveriesJson.json');
let top10EconomicalBowlers2015=require('../top10EconomicalBowlers2015.js');

let top10EconomicalBowlers=top10EconomicalBowlers2015(matches,deliveries);
console.log(top10EconomicalBowlers);

//check
let actualResult=JSON.stringify(top10EconomicalBowlers);
let expectedResult=[
  { bowler: 'RN ten Doeschate', economy_rate: 4 },
  { bowler: 'J Yadav', economy_rate: 5 },
  { bowler: 'R Ashwin', economy_rate: 6 },
  { bowler: 'S Nadeem', economy_rate: 6 },
  { bowler: 'V Kohli', economy_rate: 6 },
  { bowler: 'Sandeep Sharma', economy_rate: 7 },
  { bowler: 'Parvez Rasool', economy_rate: 7 },
  { bowler: 'GB Hogg', economy_rate: 7 },
  { bowler: 'MC Henriques', economy_rate: 7 },
  { bowler: 'MA Starc', economy_rate: 7 }
];

  if(actualResult ===JSON.stringify(expectedResult)){
      fs.writeFile('../../public/output/top10EconomicalBowlers2015.json',actualResult,function(err,data){
          if(err){
            console.log(err);

          }else{
              console.log('Data is stored at top10EconomicalBowlers2015.json file');
          }
          
      });


  }else{
      console.log('Wrong output');
  }
