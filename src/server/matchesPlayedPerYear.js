//initial value : matchesCountObject={};
    //If {season: } present in matchCountObject 
        // increment the value by 1
    //else
       // push the object {season:1} in matchesCountObject

function matchesPlayedPerYear(matches){
    let matchesCountList=matches.reduce(function(matchesCountObject,match){
        if(matchesCountObject[match['season']]){
            matchesCountObject[match['season']]+=1;
        }else{
            matchesCountObject[match['season']]=1;
        }
        return (matchesCountObject);
    },{});
    return matchesCountList;

}

module.exports=matchesPlayedPerYear;