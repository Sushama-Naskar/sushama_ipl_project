let csvToJson = require('convert-csv-to-json');

let matchesCsvPath='../data/matches.csv';
let matchesJsonPath='../data/matchesJson.json';

csvToJson.fieldDelimiter(',').formatValueByType().generateJsonFileFromCsv(matchesCsvPath,matchesJsonPath);

let deliveriesCsvPath='../data/deliveries.csv';
let deliveriesJsonPath='../data/deliveriesJson.json';

csvToJson.fieldDelimiter(',').formatValueByType().generateJsonFileFromCsv(deliveriesCsvPath,deliveriesJsonPath);

