function teamWonTossAndMatch(matches) {
    let winnerCountList = matches.reduce(function (winnerCountObject, match) {
        if (match['winner'] === match['toss_winner']) {
            if (winnerCountObject[match['winner']]) {
                winnerCountObject[match['winner']] += 1;
            } else {
                winnerCountObject[match['winner']] = 1;
            }
        }
        return winnerCountObject;
    }, {});
    return winnerCountList;
}

module.exports = teamWonTossAndMatch;