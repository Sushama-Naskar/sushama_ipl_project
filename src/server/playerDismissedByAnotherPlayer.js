

function playerDismissedByAnotherPlayer(deliveries) {
    let seasonList = deliveries.reduce(function (resultArray, delivery) {
        if (delivery['player_dismissed']) {
            let index = resultArray.findIndex(function (value) {
                return value['player_dismissed'] === delivery['player_dismissed'];
            });

            if (index != -1) {
                let playerArray = resultArray[index]['bowler'];
                let playerIndex = playerArray.findIndex(function (player) {
                    return player['bowler'] === delivery['bowler'];
                });
                if (playerIndex !== -1) {
                    playerArray[playerIndex]['count'] += 1;
                } else {
                    let obj = {};
                    obj['bowler'] = delivery['bowler'];
                    obj['count'] = 1;
                    resultArray[index]['bowler'].push(obj);
                }
            } else {
                let seasonObject = {};
                seasonObject['player_dismissed'] = delivery['player_dismissed'];
                seasonObject['bowler'] = [];
                let PlayerObject = {};
                PlayerObject['bowler'] = delivery['bowler'];
                PlayerObject['count'] = 1;
                seasonObject['bowler'].push(PlayerObject);
                resultArray.push(seasonObject);
            }

        }
        return resultArray;
    }, []).map(function (value) {
        let player = value['bowler'];
        player.sort(function (a, b) {
            return b['count'] - a['count'];
        });
        let obj = {};
        obj['player_dismissed'] = value['player_dismissed'];
        obj['bowler'] = player[0]['bowler'];
        obj['count'] = player[0]['count'];
        return obj;

    }).sort(function (a, b) {
        return b['count'] - a['count'];
    });
    return seasonList[0];
}



module.exports = playerDismissedByAnotherPlayer;