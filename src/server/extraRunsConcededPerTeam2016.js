//find all the matchid of 2016 form matchesJson.js file and stored in an array matcheIdList

//initail value:extraRunList={}
    //if match_id present in matcheIdList
        //if {bowling_team: ,extra_runs: } present in extraRunList
             //add extra runs
        //else
            //push {bowling_team: ,extra_runs: } in extraRunList    


function extraRunsConcededPerTeam2016(matches,deliveries){
    let year=2016;
    let matcheIdList=matches.filter(function(match){
        return match['season']===year;
    }).map(function(match){
        return match['id'];
    });

    let teamList=deliveries.reduce(function(extraRunList,delivery){
        if(matcheIdList.includes(delivery['match_id'])){
            if(extraRunList[delivery['bowling_team']]){
                extraRunList[delivery['bowling_team']]+=delivery['extra_runs'];
            }else{
                extraRunList[delivery['bowling_team']]=delivery['extra_runs'];
            }
        }
        return extraRunList;
    },{});

    return teamList;
}

module.exports=extraRunsConcededPerTeam2016;