//initail value: winnerCountObject={};
    //if {winner: } is present in winnerCountObject 
        // if  {winner:  ,season: } is present in winnerCountObject 
            //increment the value of season by 1;
        //else 
           //push {winner: ,season: } in winnerCountObject
    //else:
       // push {winner: ,season:1} in winnerCountObject       


function matchesWonPerTeamPerYear(matches){
    let winnerCountList=matches.reduce(function(winnerCountObject,match){
        if(winnerCountObject[match['winner']]){
            if(winnerCountObject[match['winner']][match['season']]){
                winnerCountObject[match['winner']][match['season']]+=1;
            }else{
                winnerCountObject[match['winner']][match['season']]=1;
            }

        }else{
            winnerCountObject[match['winner']]={};
            winnerCountObject[match['winner']][match['season']]=1;
        }
        return winnerCountObject;
    },{});
    return winnerCountList;
}

module.exports=matchesWonPerTeamPerYear;