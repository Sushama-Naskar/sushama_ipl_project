
function testPlayerWonHighestNumberOfPlayerOfTheMatch(matches) {
    let seasonList = matches.reduce(function (seasonArray, match) {
        let index = seasonArray.findIndex(function (value) {
            return value['season'] === match['season'];
        });

        if (index != -1) {
            let playerArray = seasonArray[index]['player_of_match'];
            let playerIndex = playerArray.findIndex(function (player) {
                return player['player'] === match['player_of_match'];
            });
            if (playerIndex !== -1) {
                playerArray[playerIndex]['count'] += 1;
            } else {
                let obj = {};
                obj['player'] = match['player_of_match'];
                obj['count'] = 1;
                seasonArray[index]['player_of_match'].push(obj);
            }
        } else {
            let seasonObject = {};
            seasonObject['season'] = match['season'];
            seasonObject['player_of_match'] = [];
            let PlayerObject = {};
            PlayerObject ['player'] = match['player_of_match'];
            PlayerObject ['count'] = 1;
            seasonObject['player_of_match'].push(PlayerObject );
            seasonArray.push(seasonObject);
        }
        return seasonArray;


    }, []).map(function (value) {
        let player = value['player_of_match'];
        player.sort(function (a, b) {
            return b['count'] - a['count'];
        });
        let PlayerObject = {};
        PlayerObject ['season'] = value['season'];
        PlayerObject ['player'] = player[0]['player'];
        return PlayerObject;


    });

   
    return seasonList;
}


module.exports = testPlayerWonHighestNumberOfPlayerOfTheMatch;