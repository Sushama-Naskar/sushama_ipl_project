//find all the matchid of 2015 form matchesJson.js file and store in matchIdList

//initial value: bowlerArray=[];
    //if match_id present in matchIdList
        //if {bowler: , total_runs: ,ball: } present in bowlerArray 
            //add the value of total_runs and ball
      //else 
           //push {bowler: , total_runs: ,ball: } in blowlerArray

//Calculate economy_rate  and store the objects {bowler: , economy_rate: } 
//sort the array top10EconomicalBowlers basesd on economy_rate
//get the first 10 objects


function top10EconomicalBowlers2015(matches,deliveries){
    let year=2015;
    let matchIdList=matches.filter(function(match){
        return match['season']===year;
        
    }).map(function(match){
        return match['id'];
    });

    let top10EconomicalBowlers=deliveries.reduce(function(bowlerArray,delivery){
        if(matchIdList.includes(delivery['match_id'])){
            let bowlerObject=bowlerArray.findIndex(function(bowler){
                return bowler['bowler']===delivery['bowler'];
            });
            if(bowlerObject!==-1){
                bowlerArray[bowlerObject]['total_runs']+=delivery['total_runs'];
                bowlerArray[bowlerObject]['ball']+=1;
            }else{
                let bowler={};
                bowler['bowler']=delivery['bowler'];
                bowler['total_runs']=delivery['total_runs'];
                bowler['ball']=1;
                bowlerArray.push(bowler);
            }

        }
        return bowlerArray;
    },[]).map(function(bowler){
        let bowlerObject={};
        bowlerObject['bowler']=bowler['bowler'];
        bowlerObject['economy_rate']=Math.ceil(bowler['total_runs']/(bowler['ball']/6));
        return bowlerObject;
    }).sort(function(a,b){
        return a['economy_rate']-b['economy_rate'];
    }).slice(0,10);
   
    
    return  top10EconomicalBowlers;
}


module.exports=top10EconomicalBowlers2015;